package com.example.itlogle;

import java.sql.*;

/**
 * Contains all methods to access the data base and get and set informations.
 *
 * @author Wiebke Wesselmann & Luis Rüfenacht
 */

public class DBQueries {

    //Constructor & Instance for Singleton
    private DBQueries() {
    }

    private static DBQueries db_instance = null;

    //Access database
    Connection con;
    Statement st;
    private final String userName = "root";
    private final String password = "";
    private final String linkDB = "jdbc:mariadb://localhost:3306/it_logle";
    private final String driver = "org.mariadb.jdbc.Driver";

    //method to create the instance
    public static DBQueries getInstance() {
        if (db_instance == null) {
            db_instance = new DBQueries();
        }
        return db_instance;
    }

    //Connecting with database
    public void connectToDB() {
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(linkDB, userName, password);
            st = con.createStatement();
            System.out.println("Connection successful");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //saves the user and the reached points in the database
    public void saveGameDataDB(String nameCurrentPlayer, int pointsCurrentPlayer) {
        String query = "INSERT INTO PLAYER (PLAYERNAME, POINTS, GESPIELT_AM) values \n" +
                "('" + nameCurrentPlayer + "'," + pointsCurrentPlayer + ",CURRENT_TIMESTAMP());";
        try {
            st = con.createStatement();
            st.execute(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //generates the ranking data
    public ResultSet getRankingList() {
        ResultSet rs = null;
        String query = "SELECT PLAYERNAME, AVG(POINTS) AS AVG_POINTS, RANK() OVER\n" +
                "(\n" + "ORDER BY avg(points) desc\n" + ")\n" +
                "MY_RANK\n" +
                "FROM PLAYER\n" +
                "GROUP BY PLAYERNAME\n" +
                "ORDER BY MY_RANK ASC;";
        try {
            rs = st.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    //randomly returns an image from the databse
    public ResultSet getLogoData() {
        ResultSet rs = null;
        String query = "SELECT img, name_to_guess FROM IMAGES\n" +
                "ORDER BY RAND()\n" +
                "LIMIT 1;\n";
        try {
            rs = st.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }
}