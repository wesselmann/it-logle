package com.example.itlogle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Contains all methods for the game itself.
 *
 * @author Wiebke Wesselmann & Luis Rüfenacht
 */


//handles the game (guesses)
public class ControllerGame {

    //classvariables
    @FXML
    private Label infoMessage;

    @FXML
    private TextField texteingabeFeld;

    @FXML
    private Label wrongInput;

    @FXML
    private ImageView logo = new ImageView();

    @FXML
    private BoxBlur boxBlur = new BoxBlur();
    //BuxBlur effect data
    private int blurHeight = 80;
    private int blurWidth = 145;
    private int blurIterations = 3;

    private String playerName;
    private String answer;

    private int points;
    private int guessCount;
    private int leftGuesses;

    private Image image;
    private DBQueries db = DBQueries.getInstance();
    private ControllerForControllers controllerForControllers;

    //Initializes the game
    public void startGame(String playerName, ControllerForControllers controllerForControllers) throws IOException, SQLException {
        this.controllerForControllers = controllerForControllers;
        this.playerName = playerName;
        leftGuesses = 5;
        displayImage();
        guessCount = 1;
        texteingabeFeld.requestFocus();
    }

    //get an image from the database for the game
    public void displayImage() throws SQLException, IOException {
        ResultSet rs = db.getLogoData();

        while (rs.next()) {
            answer = rs.getString(2);

            //image data
            java.sql.Blob blob = rs.getBlob(1);
            InputStream in = blob.getBinaryStream();
            BufferedImage bufferedImage = ImageIO.read(in);
            image = convertToFxImage(bufferedImage);
            logo.setImage(image);

            infoMessage.setText("Left guesses: " + leftGuesses);
            setBoxBlur();
        }
    }

    //blurres the image
    public void setBoxBlur() {
        boxBlur.setWidth(blurWidth);
        boxBlur.setHeight(blurHeight);
        boxBlur.setIterations(blurIterations);
        logo.setEffect(boxBlur);
    }

    //converts the BLOB (image in database) to an image
    private static Image convertToFxImage(BufferedImage image) {
        WritableImage wr = null;
        if (image != null) {
            wr = new WritableImage(image.getWidth(), image.getHeight());
            PixelWriter pw = wr.getPixelWriter();
            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = 0; y < image.getHeight(); y++) {
                    pw.setArgb(x, y, image.getRGB(x, y));
                }
            }
        }
        return new ImageView(wr).getImage();
    }

    //checks the guess
    public void submitGuess(ActionEvent e) throws IOException, SQLException {
        String guess = texteingabeFeld.getText().toUpperCase();
        texteingabeFeld.setText("");

        if (guess.equals(answer)) {
            gameEnd();
        } else if (guessCount >= 5) {
            leftGuesses -= 1;
            gameEnd();
        } else if (guess.equals("")) {
            wrongInput.setText("Please enter a guess!");
        } else {
            blurHeight = blurHeight - 16;
            blurWidth = blurWidth - 29;
            setBoxBlur();
            guessCount += 1;
            leftGuesses -= 1;
            infoMessage.setText("Left guesses: " + leftGuesses);
            wrongInput.setText("Try again!");
        }
    }

    //calculates the points
    public void calcPoints() {
        this.points = leftGuesses * 20;
    }

    //saves the player and its points in the database
    public void registerGameInDB(String playerName, int points) {
        db.saveGameDataDB(playerName, points);
    }

    //round is over --> win or lose
    public void gameEnd() throws SQLException, IOException {
        calcPoints();
        registerGameInDB(playerName, points);
        goToResults();
    }

    //switches scene to result scene
    public void goToResults() throws IOException, SQLException {
        controllerForControllers.switchToResult(points, playerName, answer, image);
    }
}