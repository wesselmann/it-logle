package com.example.itlogle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Contains all methods to coordinate all other controllers and views within the application.
 *
 * @author Wiebke Wesselmann & Luis Rüfenacht
 */


//Administration of all controllers
public class ControllerForControllers extends Application {

    //Classvariables
    private Stage stage;

    public static void main(String[] args) {
        launch(args);
    }

    //starts the GUI
    @Override
    public void start(Stage stage) {
        try {
            this.stage = stage;
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Start.fxml"));
            Parent root = fxmlLoader.load();
            Scene sceneStart = new Scene(root);

            Image icon = new Image("file://src/Logo IT-Logle.png");
            stage.getIcons().add(icon);
            stage.setTitle("IT-Logle");
            stage.setScene(sceneStart);
            stage.setX(100);
            stage.setY(0);
            stage.show();

            ControllerStart controllerStart = fxmlLoader.getController();
            controllerStart.startControllerStart(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //switches scene to gaming scene
    public void switchToGame(String playerName) throws IOException, SQLException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Game.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        ControllerGame controllerGame = fxmlLoader.getController();

        controllerGame.startGame(playerName, this);
    }

    //switches scene to result scene
    public void switchToResult(int points, String playerName, String answer, Image image) throws IOException, SQLException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Results.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        ControllerResults controllerResults = fxmlLoader.getController();

        controllerResults.startResultView(points, playerName, answer, this, image);
    }

    //switches scene to Start Menu scene
    public void switchToControllerStart() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Start.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        ControllerStart controllerStart = fxmlLoader.getController();

        controllerStart.startControllerStart(this);
    }
}