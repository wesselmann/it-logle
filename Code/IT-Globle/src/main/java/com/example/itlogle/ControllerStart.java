package com.example.itlogle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Contains all methods for the start menu of the application.
 *
 * @author Wiebke Wesselmann & Luis Rüfenacht
 */


//controlls the Start Menu
public class ControllerStart {

    @FXML
    private TextField texteingabeFeld;

    @FXML
    private Label errorMsg;

    @FXML
    private AnchorPane popUp;

    @FXML
    private Button buttonName;

    private ControllerForControllers controllerForControllers;
    private DBQueries db = DBQueries.getInstance();


    //creates the connection to the database
    public void startControllerStart(ControllerForControllers controllerForControllers) {
        this.controllerForControllers = controllerForControllers;
        db.connectToDB();
    }

    //opens the Pop-up for the player registration
    public void openNamePopUp(ActionEvent e) {
        popUp.setVisible(true);
        buttonName.setDefaultButton(true);
        texteingabeFeld.requestFocus();
    }

    //Validation of the name entry
    public void submitName(ActionEvent e) throws IOException, SQLException {
        String playerName = texteingabeFeld.getText().toUpperCase();
        if (playerName.isEmpty()) {
            errorMsg.setText("please enter a valid name");
        } else {
            popUp.setVisible(false);
            texteingabeFeld.setText("");
            controllerForControllers.switchToGame(playerName);
        }
    }

    //closes the Pop-up
    public void closeNamePopUp(ActionEvent e) {
        popUp.setVisible(false);
        texteingabeFeld.setText("");
    }
}