package com.example.itlogle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Contains all methods for the result view, which is displayed after every game round.
 *
 * @author Wiebke Wesselmann & Luis Rüfenacht
 */


//shows the ranking at the end of the game
public class ControllerResults {

    @FXML
    private Label scoreMessage;

    @FXML
    private Label playernamePos1;

    @FXML
    private Label pointsPos1;

    @FXML
    private Label playernamePos2;

    @FXML
    private Label pointsPos2;

    @FXML
    private Label playernamePos3;

    @FXML
    private Label pointsPos3;

    @FXML
    private Label positionCurrentPlayer;

    @FXML
    private ImageView logo;

    private String playerName;
    private String answer;

    private DBQueries db = DBQueries.getInstance();
    private ControllerForControllers controllerForControllers;

    //starts the result scene
    public void startResultView(int points, String playerName, String answer, ControllerForControllers controllerForControllers, Image image) throws SQLException {
        this.controllerForControllers = controllerForControllers;
        this.playerName = playerName;
        this.answer = answer;
        logo.setImage(image);
        setQuote(points);
        setRanking();
    }

    //displays the ranking
    private void setRanking() throws SQLException {
        ResultSet ranking = db.getRankingList();
        ranking.first();

        pointsPos1.setText(String.valueOf(ranking.getInt(2)));
        playernamePos1.setText(ranking.getString(1));
        ranking.next();
        pointsPos2.setText(String.valueOf(ranking.getInt(2)));
        playernamePos2.setText(ranking.getString(1));
        ranking.next();
        pointsPos3.setText(String.valueOf(ranking.getInt(2)));
        playernamePos3.setText(ranking.getString(1));

       if (playerName.equals(playernamePos1.getText()) || playerName.equals(playernamePos2.getText()) || playerName.equals(playernamePos3.getText())) {
            positionCurrentPlayer.setText(" ");
        } else {
                while (!ranking.getString(1).equals(playerName)) {
                    ranking.next();
                }

                int rank = ranking.getInt(3);
                int avgPoints = ranking.getInt(2);
                positionCurrentPlayer.setText(playerName + " YOU ARE CURRENTLY ON RANK " + rank + " WITH " + avgPoints + " AVG. POINTS");
        }
    }

    //score evaluation
    public void setQuote(int points) {
        String quoteText;

        if (points == 100) {
            quoteText = points + " points! I can smell a new legend rising to the top!";
        } else if (points == 80) {
            quoteText = points + " points! You’re on fire \uD83D\uDD25!";
        } else if (points == 60) {
            quoteText = points + " points! Pretty neat!";
        } else if (points == 0) {
            quoteText = "The right answer was " + answer + ". No worries! Nobody can know them all ;)";
        } else {
            quoteText = points + " points. You're getting there :D";
        }
        scoreMessage.setText(quoteText);
    }

    //back to Start Menu
    public void goToStart(ActionEvent e) throws IOException {
        controllerForControllers.switchToControllerStart();
    }

    //starts a new game with the same player
    public void newGame(ActionEvent e) throws IOException, SQLException {
        controllerForControllers.switchToGame(playerName);
    }
}
