module com.example.itlogle {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.desktop;


    opens com.example.itlogle to javafx.fxml;
    exports com.example.itlogle;
}