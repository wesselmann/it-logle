# IT-LOGLE

Projektdokumentation für das Schulprojekt IT-LOGLE der Module M120 und M326.

## Design und Idee
* [Anfangsdesign](/M326_WUP21a_Ruefenacht&Wesselmann_Design.pdf)
* [Update Klassendiagramm](/M326_WUP21a_Ruefenacht&Wesselmann_UpdateKlassendiagramm.png)

## Datenbank
* [DDL](/SQL/M326_WUP21a_Ruefenacht&Wesselmann_ddl.sql)
* [DML](/SQL/M326_WUP21a_Ruefenacht&Wesselmann_dml.sql)

## Testing
* [Testfallmatrix](/M326_WUP21a_Ruefenacht&Wesselmann_Testfallmatrix.pdf)
* [Testprotokoll](/M120_WUP21a_Ruefenacht&Wesselmann_Testprotokoll.pdf)